<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body {
  margin: 0;
  text-align: center;
  font-family: 'Montserrat', sans-serif;
  background: #efefef;
}

div {
  position: absolute;
  width: 100%;
  top: 50%;
  margin-top: -270px;
}

#canvas {
  border: solid 15px #fff;
  box-shadow: 0 0 15px #000;
}

p {
  font-size: 14px;
  margin: 0;
}

</style>
</head>
<body>
<div>
  <canvas id="canvas" width="500" height="500"></canvas>
  <p>Inspired by <a href="https://www.instagram.com/p/B3dHAHCHSrv/" target="_blank">davebeesbombs</a>, made with <a href="https://zzz.dog" target="_blank">zdog</a></p>
</div>


<script type="text/javascript">
"use strict";

function App() {
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var scenes = [];
  var frames = 300;
  var rspeed = Math.PI / frames;
  var sceneIndex = 0;
  var counter = 0;
  var width = 500,
      height = width,
      cx = width / 2,
      cy = height / 2;
  var n = 10,
      size = width / n;
  var dx = size * 2,
      dy = size;
  var color0 = '#EDF5E6',
      color1 = '#273C7A',
      color2 = '#E24F7B',
      color3 = '#59C3C3';
  init();

  function init() {
    var scene = new Zdog.Anchor();
    scene.objects = [];
    scene.background = color0;

    for (var i = 0; i < n; i++) {
      for (var j = 0; j < n; j++) {
        var o = new Zdog.Cylinder({
          addTo: scene,
          diameter: size,
          length: size,
          translate: {
            x: i * dx - (cx - size * (j % 2)),
            y: j * dy - (cy - size / 2)
          },
          stroke: false,
          color: color1,
          backface: color2
        });
        o.rx = j % 2 > 0 ? 1 : -1;
        scene.objects.push(o);
      }
    }

    scenes.push(scene);
    scene = new Zdog.Anchor();
    scene.objects = [];
    scene.background = color1;

    for (var _i = 0; _i < n; _i++) {
      for (var _j = 0; _j < n; _j++) {
        var _o = new Zdog.Cylinder({
          addTo: scene,
          diameter: size,
          length: size,
          translate: {
            x: _i * dx - (cx - size * (_j % 2 - 1)),
            y: _j * dy - (cy - size / 2)
          },
          stroke: false,
          color: color0,
          backface: color3
        });

        _o.rx = _j % 2 > 0 ? 1 : -1;
        scene.objects.push(_o);
      }
    }

    scenes.push(scene);
    sceneIndex = 0;
    animate();
  }

  function animate() {
    if (counter == 2 * frames) counter = 0;

    if (counter == 0) {
      sceneIndex = 0;
      resetScene(sceneIndex);
    } else if (counter == frames) {
      sceneIndex = 1;
      resetScene(sceneIndex);
    }

    var scene = scenes[sceneIndex];

    for (var i = 0; i < scene.objects.length; i++) {
      var o = scene.objects[i];
      o.rotate.x += o.rx * rspeed;
    }

    counter++;
    render(scene);
    requestAnimationFrame(animate);
  }

  function resetScene(index) {
    // reset rotation to avoid precision issue
    for (var i = 0; i < scenes[index].objects.length; i++) {
      scenes[index].objects[i].rotate.x = Math.PI / 2;
    }
  }

  function render(scene) {
    ctx.fillStyle = scene.background;
    ctx.fillRect(0, 0, width, height);
    ctx.save();
    ctx.translate(cx, cy);
    scene.updateGraph();
    scene.renderGraphCanvas(ctx);
    ctx.restore();
  }
}

App();

</script>
</body>
</html>