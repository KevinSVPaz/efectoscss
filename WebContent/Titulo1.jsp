<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="/resources/js/materialize.js">
</script>
<style>
body {
  margin: 0;
  padding: 0;
  font-family: sans-serif;}

.middle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);}

.container {
  position: relative;
  display: inline-block;
  padding: 0 40px;
  cursor: pointer;}

.text {
  font-size: 100px;
  text-transform: uppercase;
  font-weight: 900;
  position: relative;
  color: #34495e;}

.text::before {
  content: attr(data-text);
  position: absolute;
  color: #fff;
  width: 0;
  overflow: hidden;
  transition: 0.6s;}

.container::before {
  content: "";
  width: 0%;
  height: 100%;
  position: absolute;
  background: #2980b9;
  right: 0;
  top: 0;
  transition: 0.6s;}

.container:hover .text::before, .container:hover::before {
  width: 100%;}
</style>
</head>
<body>
  <div class="middle">
    <span class="container">
      <div class="text" data-text="Registro Contable">Registro Contable</div>
    </span>
  </div>
</body>
</html>