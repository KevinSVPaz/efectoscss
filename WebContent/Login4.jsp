<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<style type="text/css">
HTML CSS JSResult
@import url("https://fonts.googleapis.com/css?family=Lato:400,700&display=swap");

* {
  box-sizing: border-box;
  padding: 0;
  margin: 0;
}
body {
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='hsl(0, 0%25, 20%25)' opacity='0.1' width='20' height='20' viewBox='-5 -5 10 10'%3E%3Ccircle id='dot' r='1' /%3E%3Cuse href='%23dot' x='5' y='5' /%3E%3Cuse href='%23dot' x='-5' y='5' /%3E%3Cuse href='%23dot' x='5' y='-5' /%3E%3Cuse href='%23dot' x='-5' y='-5' /%3E%3C/svg%3E"),
    hsl(0, 0%, 95%);
  font-family: "Lato", sans-serif;
  background-size: 10px;
}
body > * + * {
  margin-top: 2rem;
}
/* svg */
svg {
  display: block;
  width: 90vw;
  max-width: 250px;
  height: auto;
  margin: 1rem;
  color: hsl(0, 0%, 10%);
}

/* when the .hide class is added to the svg scale the elements to show the closed eye */
svg.hide .eyelid,
svg.hide .brows {
  transform: scaleY(-1);
}
svg.hide .eyeball {
  transform: scale(0);
}

/* for browsers supporting the d property transition the properties to have the eyes close/open */
@supports (d: path("")) {
  svg .eyelid,
  svg .brows path,
  svg .eyeball {
    transition: transform 0.5s ease-in-out, d 0.5s ease-in-out;
  }
  svg.hide .eyelid,
  svg.hide .brows,
  svg.hide .eyeball {
    transform: initial;
  }

  svg.hide .eyelid {
    d: path("M -30 0 c 15 20 45 20 60 0");
  }
  svg.hide .eyeball {
    transform: scaleY(0);
  }
  svg.hide .brows path:first-of-type {
    d: path("M -16 12 q -5 0 -8 8");
  }
  svg.hide .brows path:nth-of-type(2) {
    d: path("M 0 16 v 8");
  }
  svg.hide .brows path:last-of-type {
    d: path("M 16 12 q 5 0 8 8");
  }
}

/* form */
/* display the elements in a column */
form {
  padding: 1.5rem 2rem;
  background: hsl(0, 0%, 100%);
  color: hsl(0, 0%, 10%);
  max-width: 300px;
  width: 95vw;
  display: flex;
  flex-direction: column;
  box-shadow: 0 1px 10px -8px currentColor;
}
form > * + * {
  margin-top: 0.5rem;
}
form label {
  display: block;
  font-weight: 700;
}
form input {
  padding: 0.5rem;
  border: 2px solid currentColor;
  background: none;
  font-size: 1rem;
  font-family: inherit;
  color: inherit;
  outline: none;
}
form button {
  background: hsl(0, 0%, 10%);
  border: none;
  color: hsl(0, 0%, 100%);
  font-size: 0.9rem;
  text-transform: uppercase;
  font-family: inherit;
  padding: 0.5rem 1rem;
}

/* for browsers supporting grid properties create the following layout
label label
input button
*/
@supports (display: grid) {
  form {
    display: grid;
    grid-template-columns: 2fr 1fr;
  }
  form label {
    grid-column: 1/-1;
  }
  form input {
    border-right: none;
    width: 100%;
  }
}
</style>
</head>
<body>
<!-- eyes -->
<svg class="hide" viewBox="-32.5 -30 140 60" width="140" height="60">
  <g>
    <g fill="none" stroke="currentColor" stroke-width="5" stroke-linecap="round">
      <!-- scaleY(-1) -->
      <g class="brows">
        <path d="M -16 -12 q -5 0 -8 -8" />
        <path d="M 0 -16 v -8" />
        <path d="M 16 -12 q 5 0 8 -8" />
      </g>
      <!-- d attribute
        M -30 0 c 15 20 45 20 60 0
        -->
      <path class="eyelid" d="M -30 0 c 15 -20 45 -20 60 0" />
      <path d="M -30 0 c 15 20 45 20 60 0" />
      <!-- transform origin -->
      <g transform="translate(0 12)">
        <!-- scaleY(0) -->
        <g class="eyeball">
          <circle r="12" cy="-12" />
        </g>
      </g>
    </g>
  </g>
  <!--  -->
  <g transform="translate(75 0)">
    <g fill="none" stroke="currentColor" stroke-width="5" stroke-linecap="round">
      <g class="brows">
        <path d="M -16 -12 q -5 0 -8 -8" />
        <path d="M 0 -16 v -8" />
        <path d="M 16 -12 q 5 0 8 -8" />
      </g>
      <path class="eyelid" d="M -30 0 c 15 -20 45 -20 60 0" />
      <path d="M -30 0 c 15 20 45 20 60 0" />
      <g transform="translate(0 12)">
        <g class="eyeball">
          <circle r="12" cy="-12" />
        </g>
      </g>
    </g>
  </g>
</svg>

<form>
  <label for="password">
    Password
  </label>
  <input type="password" id="password" value="secret" />
  <button>Show</button>
</form>

<script type="text/javascript">
const form = document.querySelector('form');
const svg = document.querySelector('svg');
const button = document.querySelector('button');

form.addEventListener('submit', (e) => {
  e.preventDefault();

});

// beside updating the text and the type attribute in the form, toggle the class of .hide on the svg to update the visual
button.addEventListener('click', () => {
  const type = form.password.getAttribute('type');
  if(type === 'password') {
    button.textContent = 'Hide';
    form.password.setAttribute('type', 'text');
    svg.classList.remove('hide')
  } else {
    button.textContent = 'Show';
    form.password.setAttribute('type', 'password');
    svg.classList.add('hide')
  }
});

</script>
</body>
</html>