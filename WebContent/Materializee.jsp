<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="/resources/css/materialize.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/resources/js/materialize.js">
</script>
<link href="https://fonts.googleapis.com/css?family=Bebas+Neue&display=swap" rel="stylesheet">
<style>
@import url('https://fonts.googleapis.com/css?family=Amatic+SC');

body {
	height: 100%;
	background-image: linear-gradient(to top, #002827 0%, #004947 100%);
	background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;
  
   margin: 0;
  padding: 0;
  font-family: sans-serif;
}


</style>
</head>
<body>
<nav>
    <div class="nav-wrapper" style="background-color: #05878a;font-family: 'Bebas Neue' ;!important">
      <a href="redirecIndex" class="brand-logo right">Registro Contable</a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li><a href="verDetalleAsiento">Asientos</a></li>
        <li><a href="verCuentas">Cuentas</a></li>
      </ul>
    </div>
  </nav>

</body>
</html>