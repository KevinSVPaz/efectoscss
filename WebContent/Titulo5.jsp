<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Lato:400,300);
  
.overlay,
.myloading {
  position: fixed;
  border-radius: 75%;
}

.overlay {
  background: rgba(0, 0, 0, .5);
  -webkit-animation-name: bubble;
  -webkit-animation-duration: 1s;
  -webkit-animation-fill-mode: forwards;
  width: 100px;
  height: 100px;
  left: calc(50% - 50px);
  top: calc(50% - 50px);
}

.myloading {
  background: #16F3C5;
  -webkit-animation-name: small-bubble;
  -webkit-animation-duration: 1s;
  -webkit-animation-fill-mode: forwards;
  width: 100px;
  height: 100px;
  left: calc(50% - 50px);
  top: calc(50% - 50px);
}

.ball {
  box-sizing: border-box;
  padding-top: 40px;
  color: white;
  text-align: center;
  text-transform: uppercase;
  font-family: 'Lato', sans-serif;
  font-size: 20px;
}

@-webkit-keyframes bubble {
  from {
    -webkit-transform: scale(1, 1);
  }
  to {
    -webkit-transform: scale(10, 10);
  }
}

@-webkit-keyframes small-bubble {
  from {
    -webkit-transform: scale(0, 0);
  }
  to {
    -webkit-transform: scale(.1, .1);
  }
}
</style>
</head>
<body>
<div class="overlay">
  <div class="myloading">
    <div class="ball">Titulo</div>
  </div>
</div>
</body>
</html>