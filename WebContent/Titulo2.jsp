<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
h2 {
	  font-weight: 300;
	  position: relative;  
	  line-height: 70px;
	  font-size: 50px;
	  display: inline;
	  clear: both;
	  text-align: left;
	  margin-bottom: 20px;
	}

	.container {
	  width: 500px;
	  margin: 20px;
	}

	h2 span {
	/*   background: linear-gradient(to right, #00a6ff 0%,#00ffce 100%);
	  box-shadow: 0.2em 0 0 rgba(#fff,0.7), -0.2em 0 0 rgba(#fff,0.7); */
	  z-index: 10;
	  position: relative;
	}

	h2 span::after {
	  content: "";
	  display: inline-block;
	  background: blue; 
	  padding: 0 .2rem;
	  position: absolute;
	  left: -4px;
	  top: 0;
	  bottom: 0;
	  right: -3px;
	  background: linear-gradient(to right, #00a6ff 0%,#00ffce 100%);
	}

	h2:hover span::after {
	  transition: all 350ms;
	  top: 100%;
	}


	h2::before {
	    content: "";
	  transition: all 350ms;

	  height: 7px;
	    position: absolute;
	    left: -4px;
	    bottom: -30px;
	    width: 0;
	    background: #00a6ff;
	    background: -moz-linear-gradient(left, #00a6ff 0%, #00ffce 100%);
	    background: -webkit-linear-gradient(left, #00a6ff 0%, #00ffce 100%);
	    background: linear-gradient(to right, #00a6ff 0%, #00ffce 100%);
	    transition: width 350ms;
	}

	h2:hover::before {
	  width: 100px;
	}
</style>
</head>
<body>
<div class="container">
  <h2>
    <span></span>
    <span>Registrar Nueva Factura</span>
    <span>Vea los productos asignados a las cuentas.</span</h2>
      
</div>
</body>
</html>