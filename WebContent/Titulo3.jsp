<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
@keyframes moveText {
	 50% {
		 transform: translateY(100%);
	}
}
 div {
	 color: #fff;
	 font-family: 'Economica', sans-serif;
	 height: 100vh;
	 display: flex;
	 align-items: center;
	 justify-content: center;
	 padding-bottom: 3em;
	 background-blend-mode: luminosity;
}
 h3 {
	 font-size: 6em;
}
 h3 span {
	 overflow: hidden;
	 vertical-align: top;
	 display: inline-block;
}
 h3 > span > span {
	 animation: moveText 3s ease infinite;
}
 
</style>
</head>
<body>
<div style="background-color: black">
  <h3><span><span>R</span></span><span><span>e</span></span><span><span>g</span></span><span><span>i</span></span><span><span>s</span></span><span><span>t</span></span><span><span>r</span></span><span><span>o</span></span><br><span><span>C</span></span><span><span>o</span></span><span><span>n</span></span><span><span>t</span></span><span><span>a</span></span><span><span>b</span></span><span><span>l</span></span><span><span>e</span></span><span><span>.</span></span>
  </h3>
</div>
</body>
</html>